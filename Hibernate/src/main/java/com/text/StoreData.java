package com.text;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StoreData {
	
public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf = cfg.buildSessionFactory();//second level cache
		
		Session session = sf.openSession();//first level cache
		
		Transaction t = session.beginTransaction();
		
		Student sobj = new Student();//TRANSIENT
		
		//sobj.setId(3);
		sobj.setStd_name("Ramanuj");
		sobj.setStd_clg("UPIT");
		sobj.setStd_city("Blr");
		
		session.save(sobj);//insert // PERSIST
		 
		//Student std = (Student)session.get(Student.class, 2);//read
		
		//std.setStd_name("Ranjan");//update
	//	std.setStd_clg("BITS");
		
	//	System.out.println(std.getId()+" "+std.getStd_name()+" "+std.getStd_clg()+" "+std.getStd_city());
		
	//	session.delete(std);//delete
		
		t.commit();
		
		session.close();//DETACHED
		
		System.out.println("Done");
	}

}



