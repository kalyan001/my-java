package com.text;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
//@Table(name = "STUDENT_DATA")
public class Student {
	
//	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	//@Column(name = "")
	private String std_name;
	
	private String std_clg;
	
	private String std_city;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStd_name() {
		return std_name;
	}

	public void setStd_name(String std_name) {
		this.std_name = std_name;
	}

	public String getStd_clg() {
		return std_clg;
	}

	public void setStd_clg(String std_clg) {
		this.std_clg = std_clg;
	}

	public String getStd_city() {
		return std_city;
	}

	public void setStd_city(String std_city) {
		this.std_city = std_city;
	}
}



